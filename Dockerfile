FROM openjdk:8-jdk-alpine3.9 
LABEL maintainer="neupane.krishna33@gmail.com"
ARG IMAGETAG
ENV ENTRY_ARG=$IMAGETAG
#COPY src /usr/src/app/src
#COPY pom.xml /usr/src/app
#COPY .git/ /usr/src/app/.git
#RUN mvn -f /usr/src/app/pom.xml clean package
#FROM gcr.io/distroless/java
COPY [ "target/assignment-$IMAGETAG.jar","entrypoint.sh","/opt/" ]
# COPY  /target/assignment-$IMAGETAG.jar","entrypoint.sh"
RUN addgroup -S krishh && adduser -S krishh -G krishh
RUN chown -R krishh:krishh /opt
# RUN useradd -ms /bin/sh krishh
# RUN chown -R krishh:krishh  "/opt"
# COPY --from=build /usr/src/app/target/assignment-$IMAGETAG.jar /usr/app/
EXPOSE 8085
#RUN java -jar assigment-$ENTRY_ARG.jar
WORKDIR "/opt/"
ENTRYPOINT  ["/bin/sh","-c","/opt/entrypoint.sh"]
